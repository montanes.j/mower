import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class MowerTest {
    
    private final Instruction instructionNorth = new Instruction("1 1 N");
    private final Instruction instructionEast = new Instruction("1 1 E");
    private final Instruction instructionSouth = new Instruction("1 1 S");
    private final Instruction instructionWest = new Instruction("1 1 W");
    private final Instruction forward = new Instruction("A");
    private final Instruction toLeft = new Instruction("G");
    private final Instruction toRight = new Instruction("D");

    Board board;
    @BeforeEach
    void initBoard(){ board = new Board(new Position(5,5));}

    @Test
    public void runInstruction_should_succeed(){
        String expected = "1 3 N";
        List<Instruction> instruction = List.of(new Instruction("1 2 N"), new Instruction("GAGAGAGAA"));
        Mower mower = new Mower(instruction, board);
        String result = mower.runInstruction();
        Assertions.assertEquals(expected, result);
    }
    @Test
    public void turnToLeft_from_North_should_succeed(){
        Mower mower = new Mower(List.of(instructionNorth, toLeft), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 1 W", result);
    }

    @Test
    public void turnToRight_from_north_should_succeed(){
        Mower mower = new Mower(List.of(instructionNorth, toRight), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 1 E", result);
    }

    @Test
    public void turnToLeft_from_South_should_succeed(){
        Mower mower = new Mower(List.of(instructionSouth, toLeft), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 1 E", result);
    }

    @Test
    public void turnToRight_from_South_should_succeed(){
        Mower mower = new Mower(List.of(instructionSouth, toRight), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 1 W", result);
    }

    @Test
    public void turnToLeft_from_East_should_succeed(){
        Mower mower = new Mower(List.of(instructionEast, toLeft), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 1 N", result);
    }

    @Test
    public void turnToRight_from_East_should_succeed(){
        Mower mower = new Mower(List.of(instructionEast, toRight), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 1 S", result);
    }

    @Test
    public void turnToLeft_from_West_should_succeed(){
        Mower mower = new Mower(List.of(instructionWest, toLeft), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 1 S", result);
    }

    @Test
    public void turnToRight_from_west_should_succeed(){
        Mower mower = new Mower(List.of(instructionWest, toRight), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 1 N", result);
    }

    @Test
    public void move_toNorth_should_succeed(){
        Mower mower = new Mower(List.of(instructionNorth, forward), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 2 N", result);
    }

    @Test
    public void move_toEast_should_succeed(){
        Position expected = new Position(2,1);
        Mower mower = new Mower(List.of(instructionEast, forward), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("2 1 E", result);
    }

    @Test
    public void move_toSouth_should_succeed(){
        Position expected = new Position(1,0);
        Mower mower = new Mower(List.of(instructionSouth, forward), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("1 0 S", result);
    }

    @Test
    public void move_toWest_should_succeed(){
        Position expected = new Position(0,1);
        Mower mower = new Mower(List.of(instructionWest, forward), board);
        String result = mower.runInstruction();
        Assertions.assertEquals("0 1 W", result);
    }

    @Test
    public void mower_dont_move_if_the_new_position_is_out_of_board(){
        List<Instruction> instructions = List.of(new Instruction("5 5 N"), forward);
        Mower mower = new Mower(instructions, board);
        Position expected = new Position(2,2);
        String result = mower.runInstruction();
        Assertions.assertEquals("5 5 N", result);
    }

}
