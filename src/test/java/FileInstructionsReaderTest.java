import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.List;

public class FileInstructionsReaderTest {

    @Test
    public void extractInstruction_should_succeed() throws FileNotFoundException {
        FileInstructionsReader fileInstructionsReader = new FileInstructionsReader("src/test/resources/file1.txt");
        List<Instruction> instruction = fileInstructionsReader.ExtractInstruction();
        Assertions.assertEquals(5, instruction.size());
    }

    @Test
    public void runInstruction_should_succeed() throws FileNotFoundException {
        String expected = "1 3 N 5 1 E";
        FileInstructionsReader fileInstructionsReader = new FileInstructionsReader("src/test/resources/file1.txt");
        fileInstructionsReader.ExtractInstruction();
        String result = fileInstructionsReader.runInstruction();
        Assertions.assertEquals(expected, result);

    }

}
