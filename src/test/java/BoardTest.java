import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BoardTest {

    @Test
    public void isPositionOutOfBound_should_succeed(){
        Board board = new Board(new Position(1,1));
        Position position = new Position(2,2);
        boolean result = board.isPositionOutOfBound(position);
        Assertions.assertTrue(result);
    }
}
