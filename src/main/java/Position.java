import lombok.Getter;

@Getter
public class Position {
    private final int x;
    private final int y;

    public Position(int x,int y){
        this.y = y;
        this.x = x;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Position){
            return x == ((Position) obj).x && y == ((Position) obj).y;
        }
        return false;
    }
}
