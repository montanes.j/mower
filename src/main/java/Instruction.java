import lombok.Getter;

@Getter
public class Instruction {
    private final String instruction;

    public Instruction(String instruction) {
        this.instruction = instruction;
    }
}
