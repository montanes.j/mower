import lombok.Getter;

@Getter
public class MowerPosition extends Position {

    private Orientation orientation;

    public MowerPosition(int x, int y, Orientation orientation) {
        super(x, y);
        this.orientation = orientation;
    }


    public MowerPosition turnToLeft() {
        return new MowerPosition(this.getX(), super.getY(), orientation.turnToLeft());
    }

    public MowerPosition turnToRight() {
        return new MowerPosition(this.getX(), super.getY(), orientation.turnToRight());
    }

    public MowerPosition move() {
        switch (orientation){
            case N: return  new MowerPosition(super.getX(), super.getY()+1, orientation);
            case E: return  new MowerPosition(super.getX()+1, super.getY(), orientation);
            case S: return  new MowerPosition(super.getX(), super.getY()-1, orientation);
            case W: return  new MowerPosition(super.getX()-1, super.getY(), orientation);
            default:
                throw new IllegalArgumentException("this orientation doesnt exist");
        }
    }
}
