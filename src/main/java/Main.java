import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        if(args.length != 2 && !args[0].equals("--f")){
            usage();
        }
        FileInstructionsReader fileInstructionsReader = new FileInstructionsReader(args[1]);
        fileInstructionsReader.ExtractInstruction();
        System.out.println(fileInstructionsReader.runInstruction());
    }

    private static void usage() {
        System.out.println("usage: --f pathFile");
    }
}
