import java.util.List;

public class Mower {

    private static final char A = 'A';
    private static final char G = 'G';
    private MowerPosition position;
    private final Board board;
    private final InstructionMower instructionMower;
    public Mower(List<Instruction> instructions, Board board) {
        instructionMower = new InstructionMower(instructions);
        this.board = board;
    }

    public String runInstruction(){
        position = instructionMower.getInitPosition();
        char ch = instructionMower.getNext();
        while(ch!='\n' ){
            if(A == ch ){
                move();
            }
            else if(G == ch){
                turnToLeft();
            }
            else {
                turnToRight();
            }
            ch = instructionMower.getNext();
        }
        return position.getX()+" "+ position.getY()+" "+position.getOrientation();
    }

    private void turnToLeft() {
        position = position.turnToLeft();
    }

    private void turnToRight() {
        position = position.turnToRight();
    }

    private void move() {
        MowerPosition newPosition = position.move();
        if(!board.isPositionOutOfBound(newPosition)){
            position = newPosition;
        }
    }
}
