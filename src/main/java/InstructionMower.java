import java.util.ArrayList;
import java.util.List;

public class InstructionMower {

    private MowerPosition initPosition;
    private final List<Character> forward;

    public InstructionMower(List<Instruction> instructions){
        forward = new ArrayList<>();
        instructions.forEach(instruction -> {
            if(instruction.getInstruction().matches("[AGD]*")){
                for(char ch : instruction.getInstruction().toCharArray()){
                    forward.add(ch);
                }
            }
            else if(instruction.getInstruction().matches("\\d \\d [NESW]")){
                String[] init = instruction.getInstruction().split(" ");
                initPosition = new MowerPosition(Integer.parseInt(init[0]), Integer.parseInt(init[1]), Orientation.valueOf(init[2]));
            }

        });
    }

    public MowerPosition getInitPosition(){
        return initPosition;
    }
    public Character getNext(){
        if(forward.size() > 0) {
            return forward.remove(0);
        }
        return '\n';
    }
}
