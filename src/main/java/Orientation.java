public enum Orientation {
    N("W","E"),
    E("N","S"),
    S("E","W"),
    W("S","N");


    private final String ToLeft;
    private final String toRight;

    public Orientation turnToLeft() {
        return Orientation.valueOf(ToLeft);
    }

    public Orientation turnToRight() {
        return Orientation.valueOf(toRight);
    }

    Orientation(String toLeft,String toRight) {
        this.ToLeft = toLeft;
        this.toRight = toRight;
    }
}
