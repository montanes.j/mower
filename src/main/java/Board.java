public class Board {

    private final Position boardLimit;
    public Board(Position boardLimit) {
        this.boardLimit = boardLimit;
    }

    public boolean isPositionOutOfBound(Position position) {
        return position.getX() > boardLimit.getX() || position.getX() < 0 || position.getY() > boardLimit.getY() || position.getY() < 0 ;
    }
}
