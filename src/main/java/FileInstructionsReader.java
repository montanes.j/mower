import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileSystemException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileInstructionsReader {

    private final List<Instruction> instructions;
    private final List<Instruction> mowerInstructions;
    private final List<Mower> mowers;
    private final String path;
    private Board board;
    public FileInstructionsReader(String path) {
        this.path = path;
        this.instructions = new ArrayList<>();
        this.mowerInstructions = new ArrayList<>();
        this.mowers = new ArrayList<>();
    }

    public List<Instruction> ExtractInstruction() throws FileNotFoundException {

        Scanner scanner = new Scanner(new File(path));

        while(scanner.hasNext()){
            instructions.add(new Instruction(scanner.nextLine()));
        }
        scanner.close();
        return instructions;
    }



    public String runInstruction() {
        instructions.forEach(line -> {
            try {
                InitInstruction(line);
            } catch (FileSystemException e) {
                throw new RuntimeException(e);
            }
        });
        return run();
    }

    private String run(){
        StringBuilder result = new StringBuilder();
        mowers.forEach(mower -> {
            if(!result.isEmpty()) {
                result.append(" ");
            }
            result.append(mower.runInstruction());
        });
        return result.toString();
    }
    private void InitInstruction(Instruction instruction) throws FileSystemException {
        if(instruction.getInstruction().matches("[AGD]*")){
           mowerInstructions.add(instruction);
           mowers.add(new Mower(List.copyOf(mowerInstructions), board));
           mowerInstructions.clear();
        }
        else if(instruction.getInstruction().matches("\\d \\d")){
            processLineBoard(instruction.getInstruction());
        }
        else if(instruction.getInstruction().matches("\\d \\d [NESW]")){
            if(mowerInstructions.size() >0){
                mowers.add(new Mower(List.copyOf(mowerInstructions), board));
                mowerInstructions.clear();
            }
            mowerInstructions.add(instruction);
        }
        else {
            throw new FileSystemException("wrong Format");
        }
    }

    private void processLineBoard(String line){
        String[] boardLimit = line.split(" ");
        board = new Board(new Position(Integer.parseInt(boardLimit[0]),Integer.parseInt(boardLimit[1])));
    }
}
